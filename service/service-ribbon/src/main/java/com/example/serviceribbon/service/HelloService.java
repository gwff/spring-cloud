package com.example.serviceribbon.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HelloService {
    @Autowired
    RestTemplate restTemplate;

    /**
     *  @HystrixCommand 是熔断器注解，当服务不可用时调用helloError方法
     * @param name
     * @return
     */
    @HystrixCommand(fallbackMethod = "helloError")
    public String hello(String name) {
        return restTemplate.getForObject("http://SERVICE-HELLO/hello?name="+name,String.class);
    }

    public String helloError(String name){
        return "Hello," + name + ",sorry,error!";
    }
}
