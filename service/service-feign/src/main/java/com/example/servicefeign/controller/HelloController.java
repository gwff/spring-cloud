package com.example.servicefeign.controller;

import com.example.servicefeign.controller.service.HelloFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    HelloFeignService helloFeignService;

    @GetMapping("/hello")
    public String sayHi(@RequestParam String name){
        return helloFeignService.sayHelloFromClientOne(name);
    }
}
