package com.example.servicefeign.controller.service;

import com.example.servicefeign.controller.service.feign.HelloServiceHystric;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "service-hello", fallback = HelloServiceHystric.class)
public interface HelloFeignService {
    @GetMapping("/hello")
    String sayHelloFromClientOne(@RequestParam String name);
}
