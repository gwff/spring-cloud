package com.example.servicefeign.controller.service.feign;

import com.example.servicefeign.controller.service.HelloFeignService;
import org.springframework.stereotype.Component;

@Component
public class HelloServiceHystric implements HelloFeignService {
    @Override
    public String sayHelloFromClientOne(String name) {
        return "sorry，" + name;
    }
}
