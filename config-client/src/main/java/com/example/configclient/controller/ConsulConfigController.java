package com.example.configclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope //开启更新功能
@RestController
public class ConsulConfigController {
    @Value("${zuowenjun.motto}")
    private String env;

    @GetMapping("/version")
    public String version(){
        System.out.println(env);
        return env;
    }
}
